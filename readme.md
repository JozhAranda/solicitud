## Node.js - MongoDB - Express

Debes tener descargado e instalado lo siguiente: 

Versión estable - [Nodejs](http://nodejs.org/)
Versión estable - [Express](http://expressjs.com/)
Versión estable - [MongoDB](http://www.mongodb.org/)

Node.js es una plataforma construida sobre **Chrome's JavaScript runtime** para facilitar la construcción rápida y escalable de aplicaciones en red. Node.js se basa en un manejador de eventos, modelo non-blocking I/O que lo hace más ligero y eficiente, perfecto para manejo de datos intensivo y aplicaciones en tiempo real que se comparten entre diferentes dispositivos.

Express es un minimalista y flexible framework para aplicaciones web con Node.js que provee de robustez y un set de opciones para la web y aplicaciones móviles.

MongoDB es un sistema de base de datos NoSQL orientado a documentos, desarrollado bajo el concepto de código abierto. MongoDB forma parte de la nueva familia de sistemas de base de datos NoSQL.

###Instalación

1. Descarga o clona el repositorio.
2. Antes de comenzar a hacer magia, inicia corriendo tu servicio de MongoDB. Ejemplo:
	``mongodb.exe`` y ``mongo.exe`` 
3. Usando tu terminal ingresa a la carpeta e ingresa el comando ``npm install && node app.js`` con esto instalara todas las dependencias alojadas en ``package.json`` y posterior levantara el servicio.  Si ves un mensaje como este en tu terminal ``The magic happens on port 5000``, todo ha salido excelente (no olvides correr el servicio MongoDB).
4. Ya tendrás la el servicio arriba y corriendo.

###MongoDB

Dentro de la carpeta ``config/database.js`` podrás ver la ruta que manda a llamar a MongoDB, el puerto y el nombre de la base de datos que se esta usando en este caso ``solicitudes``. 

Dentro de la carpeta ``app/models/user.js`` se encuentra el schema de la base datos en este caso en MongoDB.

####Comandos clasicos en MongoDB

 - **show dbs** - permite ver todas las base de datos
 - **use database** - selecciona una base de datos para trabajar con ella
 - **show collections** - ver la colección de datos dentro de la BD
 - **db.collection.find()** - ver los registros dentro de una colección.