Paso para la instalación y uso es el siguiente:

 - Clonar el repo donde tu desees
 - Mediante tu terminal, consola o bash dirigete a la carpeta donde se encuentra tu repo clonado
 - Dentro del repo clonado teclea dentro de tu bash: npm install
 - Posterior teclea en tu bash: node app.js

Recuerda que debes tener lo siguiente instalado:
 - NodeJS
 - NPM
 - Express
 - MongoDB  